const {app, BrowserWindow, ipcMain} = require('electron');
const Store = require('electron-store');
const path = require('path');
let store = new Store();

// Create some data, for the moment they are not persisted
let win;
let winExpense = null, winIncome = null;
let expense = [];
let income = [];
let total = [];

//create main window
function createWindow() {
    win = new BrowserWindow({width: 1000, height: 800, webPreferences:{nodeIntegration: true}});
    win.loadFile('./src/index.html');
    // Listen when the window is finished to load
    win.webContents.once('did-finish-load', function() {
        if (store.has('expense')) {
            expense = store.get('expense');
        }
        
        win.webContents.send('initExpenseTable', expense);
        if (store.has('income')) {
            income = store.get('income');
        }
        // Send the operation with initOperationtable event
        win.webContents.send('initIncomeTable', income);
        win.webContents.send('initTotalTable', total);
    })
}

// Create the main window when the app is ready
app.on('ready', createWindow);


/************************ EXPENSES *********************/
// create the new expense window
ipcMain.on('addExpense', function(e, arg) {
    if (winExpense) {
        return
    }
    winExpense = new BrowserWindow({width: 800, height: 600, webPreferences:{nodeIntegration: true}});
    winExpense.loadFile(path.join('src','addExpense.html'));
    // Listen when the window will be closed for destroy it
    winExpense.on('closed', function() {
        winExpense = null;
    })
})

//listen on expenseObject and create newExpense object
ipcMain.on('expenseObject', function(e, arg) {
    let newId = 1;
    if (expense.length > 0) {
        newId = expense[expense.length-1].id + 1
    }
    let newExpense = {
        id: newId,
        date: arg.date,
        entitled: arg.entitled,
        value: arg.value
        
    }
    //add to expense array
    expense.push(newExpense);
    //update expense store
    store.set('expense', expense);
    //send newExpense on initExpenseTable
    win.webContents.send('initExpenseTable', [newExpense]);
})

//listen on deleteExpense
ipcMain.on('deleteExpense', function(e, arg) {
    //delete from expense array
    for (let i = 0; i < expense.length; i++) {
        if (expense[i].id === arg.id) {
            expense.splice(i, 1);
            break;
        }
    }
    //update expense store
    store.set('expense', expense)
    //send arg on returnDeleteExpense
    e.sender.send('returnDeleteExpense', arg)
}) 



/************************ INCOMES *********************/
// create the new income window
ipcMain.on('addIncome', function(e, arg) {
    if (winIncome) {
        return
    }
    winIncome = new BrowserWindow({width: 800, height: 600, webPreferences:{nodeIntegration: true}});
    winIncome.loadFile(path.join('src','addIncome.html'));
    // Listen when the window will be closed for destroy it
    winIncome.on('closed', function() {
        winIncome = null;
    })
})

//listen on incomeObject and create newIncome object
ipcMain.on('incomeObject', function(e, arg) {
    let newId = 1;
    if (income.length > 0) {
        newId = income[income.length-1].id + 1
    }
    let newIncome = {
        id: newId,
        date: arg.date,
        entitled: arg.entitled,
        value: arg.value
        
    }
    //add ton income array
    income.push(newIncome);
    //update income store
    store.set('income', income);
    //send newIncom on initIncomeTable
    win.webContents.send('initIncomeTable', [newIncome]);
})

//listen on deleteIncome
ipcMain.on('deleteIncome', function(e, arg) {
    //delete from income array
    for (let i = 0; i < income.length; i++) {
        if (income[i].id === arg.id) {
            income.splice(i, 1);
            break;
        }
    }
    //update income store
    store.set('income', income)
    //send arg on returnDeleteIncome
    e.sender.send('returnDeleteIncome', arg)
}) 
