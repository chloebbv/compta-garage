const {ipcRenderer} = require('electron')
var $ = require('jquery')

//add event on submit
$('#addIncomeForm').on('submit', function(e) {
    e.preventDefault();
    let date = e.target[0].value;
    let entitled = e.target[1].value;
    let value = parseFloat(e.target[2].value);
    //verification type of value
    if (isNaN(value)) {
        //if value is Nan display message & close window
        let options = {
            type: 'warning',
            title: 'La valeur doit être un nombre',
            message: "La valeur saisie doit être un nombre",
            buttons: ["Quitter"]
        }
        dialog.showMessageBoxSync(options);
        window.close();
    }
    let newIncome = {date, entitled, value};
    //send object newExpense with event expenseObject
    ipcRenderer.send('incomeObject', newIncome);
    //reset form
    document.querySelector('form').reset();
    //close the window
    window.close()
})