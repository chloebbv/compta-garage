const {ipcRenderer} = require('electron');
let $ = require('jquery')
let {dialog} = require('electron').remote
let sum = 0;


/************* EXPENSES ************/
//function create new row in expense table
function createExpenseRow(expense) {
    let expenseTBody = $('#expenseTable tbody');
    let expenseTr = $('<tr id="rowExpense_' + expense.id + '">')
    expenseTr.append('<td>' + expense.date + '</td>');
    expenseTr.append('<td>' + expense.entitled + '</td>');
    expenseTr.append('<td>' + expense.value + '</td>');
    expenseTr.append('<td><button id="deleteExpense_' + expense.id +'">⛔ Supprimer</button></td>');
    expenseTBody.append(expenseTr);
    //add event on delete button
    $('#deleteExpense_' + expense.id).on('click', function(e) {
        e.preventDefault();
        //display confirm message to delete
        let options = {
            type: 'warning',
            title: 'Confirmation',
            message: "Souhaitez-vous vraiment supprimer cette dépense?",
            buttons: ["Oui", "Non"]
        }
        var response = dialog.showMessageBoxSync(options);
        //if yes -> send expense with event deleteExpense
        if(response == 0) {
            ipcRenderer.send('deleteExpense', expense)
        }
        
    })

}

//listen on initExpenseTable
ipcRenderer.on('initExpenseTable', function(e, arg) {
    //create row for each element of arg
   arg.forEach(createExpenseRow);
    //minus sum of each expense element value
   for (let i = 0; i < arg.length; i++) {
       sum -= arg[i].value
   }
   diplaySum(sum)
})

//add event on addExpense button
$('#addExpense').on('click', function(e) {
    e.preventDefault();
    //send addExpense event
    ipcRenderer.send('addExpense', null);
})

//listen on returDeleteExpense
ipcRenderer.on('returnDeleteExpense', function(e, arg) {
    //if expense element is delete sum add value
    sum += arg.value;
    diplaySum(sum)
    //remove the row
    $('#rowExpense_' + arg.id).remove();
    
})





/************* INCOMES ************/
//function create new row on income table
function createIncomeRow(income) {
    let incomeTBody = $('#incomeTable tbody');
    let incomeTr = $('<tr id="rowIncome_' + income.id + '">')
    incomeTr.append('<td>' + income.date + '</td>');
    incomeTr.append('<td>' + income.entitled + '</td>');
    incomeTr.append('<td>' + income.value + '</td>');
    incomeTr.append('<td><button id="deleteIncome_' + income.id +'">⛔ Supprimer</button></td>');
    incomeTBody.append(incomeTr);
    //add event on delete button
    $('#deleteIncome_' + income.id).on('click', function(e) {
        e.preventDefault();
        //display confirm message to delete
        let options = {
            type: 'warning',
            title: 'Confirmation',
            message: "Souhaitez-vous vraiment supprimer cette dépense?",
            buttons: ["Oui", "Non"]
        }
        var response = dialog.showMessageBoxSync(options);
        //if yes -> send expense with event deleteIncome
        if(response == 0) {
            ipcRenderer.send('deleteIncome', income);
        }
        
    })

}

//listen on initIncomeTable
ipcRenderer.on('initIncomeTable', function(e, arg) {
    //create row for each element of arg
   arg.forEach(createIncomeRow);
   //add sum of each income element value
   for (let i = 0; i < arg.length; i++) {
        sum += arg[i].value
    }
    diplaySum(sum)
})

//add event on addIncome button
$('#addIncome').on('click', function(e) {
    e.preventDefault();
    //send addIncome event
    ipcRenderer.send('addIncome', null);
})

//listen on returDeleteIncome
ipcRenderer.on('returnDeleteIncome', function(e, arg) {
    //if incom!e element is delete sum minus value
    sum -= arg.value;
    diplaySum(sum)
    //remove the row
    $('#rowIncome_' + arg.id).remove();
})

/************* TOTAL ************/

//show sum
function diplaySum(sum) {
    console.log(sum)
    let resultP = document.querySelector('#result p strong');
    if (sum > 0) {
        resultP.innerHTML = sum + '€<br><br> Vous êtes bénéficiaire &#128526;';
    } else if (sum < 0) {
        resultP.innerHTML = sum + '€<br><br> Vous êtes déficitaire &#128557;';
    } else  {
        resultP.innerHTML = sum + '€<br><br> Vous résultat est nul &#128528;';
    }
}
